// [1]  createSelector(s1, s2, ... sN, fn)
// [2]  createSelector(fn)
// [3]  createSelector() === undefined
// [4]  recalculating function gets called with results of selectors ... createSelector(selectMeat, selectPotato, (meat, potato) => meat * potato)
function createSelector() {
    let cachedResult = null;
    let previousResults = [];

    // [3]
    if (arguments.length === 0) {
        return undefined;
    } else {

        // [1][2]
        let selectorArray = [];
        if (arguments.length > 1) {
            selectorArray = Array.from(arguments).slice(0, arguments.length - 1);
        }
        // [1]
        let fn = arguments[arguments.length - 1];

        function recalculate(inputData) {
            let shouldRecalculate = false;
            {
                for (let i = 0; i < selectorArray.length; i++) {
                    let y = selectorArray[i](inputData);

                    let isInitialized = previousResults.length;
                    if (!isInitialized) {
                        shouldRecalculate = true;
                        previousResults.push(y);
                    } else if (y !== previousResults[i]) {
                        shouldRecalculate = true;
                        previousResults[i] = y;
                    }
                }
            }

            if (shouldRecalculate) {
                console.log(`%crecalculating: %c${fn.toString()}`, 'color:red', 'color:grey');
                cachedResult = fn(...previousResults);
                // [4]
                return cachedResult;
            } else {
                console.log(`%cgetting result from cache: %c${fn.toString()}`, 'color:green', 'color:grey');
                return cachedResult;
            }
        }

        return recalculate;
    }
}

// test from https://github.com/reduxjs/reselect
(function() {

    const shopItemsSelector = state=>state.shop.items
    const taxPercentSelector = state=>state.shop.taxPercent

    const subtotalSelector = createSelector(shopItemsSelector, items=>items.reduce((acc,item)=>acc + item.value, 0))

    const taxSelector = createSelector(subtotalSelector, taxPercentSelector, (subtotal,taxPercent)=>subtotal * (taxPercent / 100))

    const totalSelector = createSelector(subtotalSelector, taxSelector, (subtotal,tax)=>({
        total: subtotal + tax
    }))

    let exampleState = {
        shop: {
            taxPercent: 8,
            items: [{
                name: 'apple',
                value: 1.20
            }, {
                name: 'orange',
                value: 0.95
            }, ]
        }
    }

    let test = ()=>{
        console.log(subtotalSelector(exampleState))
        // 2.15
        console.log(taxSelector(exampleState))
        // 0.172
        console.log(totalSelector(exampleState))
        // { total: 2.322 }
    }
    test();
    test();
    test();
}
)();
